#!/usr/bin/env bash

INSTANCE_TAG="$STRING_PARAMETER_TAG"
REGION=eu-central-1
STACK_NAME="$STRING_PARAMETER_STACK_NAME"

aws cloudformation create-stack --stack-name $STACK_NAME --template-body file://$PWD/jenkinsawscloudformation/stack.yml --region $REGION

echo "waiting 30 seconds"
sleep 30

aws cloudformation describe-stack-resources --stack-name $STACK_NAME

INSTANCE=$(aws ec2 describe-instances --filters "Name=tag:JenkinsSlaveType,Values=$INSTANCE_TAG" "Name=instance-state-code,Values=16" \
--output text --query 'Reservations[*].Instances[*].PublicIpAddress')

INSTANCE_PUBLIC_DNS=$(aws ec2 describe-instances --filters "Name=tag:JenkinsSlaveType,Values=$INSTANCE_TAG" "Name=instance-state-code,Values=16" \
--output text --query 'Reservations[*].Instances[*].PublicDnsName')

echo "$INSTANCE"
echo "$INSTANCE_PUBLIC_DNS"

#export public ip address to the file in order to determinate which instance should be provisioned
echo "$INSTANCE" > /tmp/ip_file
#export tag name to the file in order to use in the next jobs
echo "$INSTANCE_TAG" > /tmp/tag_file
#export public dns name in order to display it on final step of deploying web application
echo "$INSTANCE_PUBLIC_DNS" > /tmp/public_dns_file
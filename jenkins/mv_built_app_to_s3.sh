#!/usr/bin/env bash

s3_name=mvn-artifacts-673733667

aws s3 mb s3://$s3_name --region eu-central-1

aws s3 mv servletDemo/target/servletDemo.war s3://$s3_name/servletDemo.war
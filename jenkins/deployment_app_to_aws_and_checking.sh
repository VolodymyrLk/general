#!/usr/bin/env bash

#variables
ip_adress=$(cat /tmp/ip_file | head -1)
s3_name=mvn-artifacts-673733667
# artifacts_local_directory=/tmp

#currently not using, because of copying *.war file directly: s3 bucket > newely created instance
# #copying *.war from s3 bucket
# aws s3 cp s3://$s3_name/servletDemo.war $artifacts_local_directory/servletDemo.war
# #copying the build *.war file to the remote instance
# scp -r $artifacts_local_directory/servletDemo.war ubuntu@$ip_adress:/opt/tomcatwars/

#in order to deploy *.war file on remote instance we're running a script on remotely, using ssh connection and EOT option
ssh -o StrictHostKeyChecking=no ubuntu@$ip_adress "$( cat <<'EOT'
apache_tomcat=/opt/tomcat/apache-tomcat-8.5.32
aws s3 cp s3://mvn-artifacts-673733667/servletDemo.war /opt/tomcatwars/servletDemo.war
sudo mv /opt/tomcatwars/servletDemo.war $apache_tomcat/webapps/
echo "tomcat restarting"
sudo bash $apache_tomcat/bin/shutdown.sh
sudo bash $apache_tomcat/bin/startup.sh
echo "i am sleeping 10 sec"
sleep 10
status_code=$(wget --spider -S "http://127.0.0.1:8080/servletDemo/" 2>&1 | grep "HTTP/" | awk '{print $2}')
echo "-------------------"
echo $status_code
if [[ "$status_code" -ne 200 ]]; then
  echo $status_code is not ok
  exit 1
fi
EOT
)"

#displaying the IP address of the deployed application
echo "public DNS address:"; cat /tmp/public_dns_file | head -1

#deleting unnecessary tmp files
rm -rf /tmp/ip_file
rm -rf /tmp/tag_file
rm -rf /tmp/public_dns_file
# sudo rm -rf $artifacts_local_directory/servletDemo.war
#removing all of the objects in the bucket and then remove the bucket itself
aws s3 rb s3://$s3_name --force

echo "in order to delete cloudformation stack you can use the following example: aws cloudformation delete-stack --stack-name apache-tomcat"
#!/bin/bash

echo ${JOB_NAME}

INSTANCE_TAG="`echo $JOB_NAME | awk -F '_' '{ print $2 }'`"

echo ${INSTANCE_TAG}

aws ec2 run-instances --region eu-central-1 --image-id ami-7c412f13 --count 1 --instance-type t2.micro --key-name jenkins --security-group-ids sg-c9e97fa4 --subnet-id subnet-c7168fbd --tag-specifications 'ResourceType=instance,Tags=[{Key=JenkinsSlaveType,Value='''$INSTANCE_TAG'''}]'
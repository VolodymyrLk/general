#!/usr/bin/env bash

#in order to run provision from ansible working directory we're moving on
cd ansible/ec2ubuntu1404ansible/
#getting IP address which has to be provisioned
ip_adress=$(cat /tmp/ip_file | head -1)
#in order to recognize tag-roles by ansible we're replacing low dashes by commas
sed -i "s/_/,/g" /tmp/tag_file
#getting tags from the first line, cleaning up from uppercase
tags=$(cat /tmp/tag_file | tr '[:upper:]' '[:lower:]' | head -1)
#replacing default value 'localhost' by our host IP address, which we're going to provision
sed -i "s/localhost/$ip_adress/g" hosts
#running ansible
ansible-playbook -i hosts ec2provision-playbook.yml --tags "general,$tags"
#!/usr/bin/env bash

echo "apache installation started"
apt-get update
apt-get install -y apache2
# if ! [ -L /var/www ]; then
  # rm -rf /var/www
  # ln -fs /vagrant /var/www
# fi
echo "apache installation processed"

echo "jenkins installation started"
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -y jenkins
echo "jenkins installation processed"

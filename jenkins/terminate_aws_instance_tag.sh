#!/bin/bash

echo ${JOB_NAME}

INSTANCE_TAG="`echo $JOB_NAME | awk -F '_' '{ print $2 }'`"

echo ${INSTANCE_TAG}

aws ec2 terminate-instances --instance-ids $(aws ec2 describe-instances --query 'Reservations[*].Instances[*].[Placement.AvailabilityZone, State.Name, InstanceId]' --filter Name=tag:JenkinsSlaveType,Values=$INSTANCE_TAG --output text | grep -v terminated | awk '{print $3}')
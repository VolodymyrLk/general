#!/usr/bin/env bash

# the sequence of actions:
# 1. create jenkins job with any name
# 2. set the flag on "This project is parameterized" checkbox
# 3. create 3 string parameters:
#   a. Name: STRING_PARAMETER_QUANTITY, Default Value: 1, Description: e.g - quantity of instances which we are creating
#   b. Name: STRING_PARAMETER_INSTANCE_TYPE, Default Value: t2.micro, Description: e.g. - type of instance; can be: t2.nano, t2.micro, t2.small, t2.medium, t2.large, t2.xlarge, t2.2xlarge.
#   c. Name: STRING_PARAMETER_IMAGE_ID_AMI, Default Value: e.g. ami-7c412f13 (Ubuntu Server 16.04 LTS), Description: e.g. - default image-id ami
#   d. Name: STRING_PARAMETER_TAG, Default Value: e.g. ApacheTomcat, Description: e.g. - default tag of instance
# 4. set up source code management: Source Code Management/Repositories/Repository URL - paste: https://VolodymyrLk@bitbucket.org/VolodymyrLk/general.git, specify appropriate branch: Branches to build/Branch Specifier (blank for 'any'): */master
# 5. add build step which called "Execute shell" and the following script into the code field:
#    #!/usr/bin/env bash
#
#    echo ${JOB_NAME}
#
#    bash /var/lib/jenkins/workspace/${JOB_NAME}/jenkins/launch_aws_instance_parameterized_bash_getopts.sh

## set debug mode
#set -x

## output log of userdata to /var/log/user-data.log
#exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# initialize variables to default values
INSTANCE_QUANTITY="$STRING_PARAMETER_QUANTITY" ## default quantity of instances which we are creating
INSTANCE_TYPE="$STRING_PARAMETER_INSTANCE_TYPE" ## default type of instance
IMAGE_ID_AMI="$STRING_PARAMETER_IMAGE_ID_AMI" ## default image-id ami
INSTANCE_TAG="$STRING_PARAMETER_TAG" ## default tag of instance
SECURITY_GROUP_IDS="$STRING_PARAMETER_SECURITY_GROUP_IDS"
SUBNET_ID="$STRING_PARAMETER_SUBNET_ID"

# help function
function HELP {
  echo "the number of arguments must be greater than 0"
  echo "---------------"
  echo "possible arguments:"
  echo "-h - showing help and exit"
  echo "-c - quantity of instances which we are creating"
  echo "-t - type of instance; can be: t2.nano, t2.micro, t2.small, t2.medium, t2.large, t2.xlarge, t2.2xlarge"
  echo "-i - image-id ami"
  echo "-v - tag of instance"
  exit 1
}

## check the number of arguments. If none are passed, print help and exit.
#NUMARGS=$#
#echo -e \\n"number of arguments: $NUMARGS"
#if [ $NUMARGS -eq 0 ]; then
#  HELP
#fi

# getting values of arguments at runtime
while getopts :hc:t:i:v: FLAG; do
    case $FLAG in
        h)  #show help
            HELP
            ;;
        c)  #set option "c"
            INSTANCE_QUANTITY=$OPTARG
            echo "-c used: $OPTARG"
            echo "INSTANCE_QUANTITY = $INSTANCE_QUANTITY"
            ;;
        t)  #set option "t"
            INSTANCE_TYPE=$OPTARG
            echo "-t used: $OPTARG"
            echo "INSTANCE_TYPE = $INSTANCE_TYPE"
            ;;
        i)  #set option "i"
            INSTANCE_TAG=$OPTARG
            echo "-i used: $OPTARG"
            echo "IMAGE_ID_AMI = $IMAGE_ID_AMI"
            ;;
        v)  #set option "v"
            INSTANCE_TAG=$OPTARG
            echo "-v used: $OPTARG"
            echo "INSTANCE_TAG = $INSTANCE_TAG"
            ;;
        \?) #unrecognized option - show help
            echo "option -$OPTARG not allowed"
            HELP
            ;;
        :)
            echo "option -$OPTARG requires an argument"
            HELP
            ;;
    esac
done

shift $((OPTIND-1))  #this tells getopts to move on to the next argument

echo "---------------"
echo "quantity of aws ec2 instances: "$INSTANCE_QUANTITY
echo "type of instance: "$INSTANCE_TYPE
echo "instance ami: "$IMAGE_ID_AMI
echo "instance tag: "$INSTANCE_TAG
echo "---------------"

EC2_INSTANCE_ID=`aws ec2 run-instances --region eu-central-1 --image-id $IMAGE_ID_AMI \
--count $INSTANCE_QUANTITY \
--instance-type $INSTANCE_TYPE \
--key-name jenkins \
--security-group-ids $SECURITY_GROUP_IDS \
--subnet-id $SUBNET_ID \
--tag-specifications 'ResourceType=instance,Tags=[{Key=JenkinsSlaveType,Value='$INSTANCE_TAG'}]' | jq -r '.Instances[].InstanceId'`

echo "EC2 InstanceID: "$EC2_INSTANCE_ID
echo "---------------"

echo "waiting for instance-running"
aws ec2 wait instance-running --instance-ids $EC2_INSTANCE_ID
echo "instance is running"
echo "---------------"

INSTANCE=$(aws ec2 describe-instances --filters "Name=tag:JenkinsSlaveType,Values=$INSTANCE_TAG" "Name=instance-state-code,Values=16" \
--output text --query 'Reservations[*].Instances[*].PublicIpAddress')

INSTANCE_PUBLIC_DNS=$(aws ec2 describe-instances --filters "Name=tag:JenkinsSlaveType,Values=$INSTANCE_TAG" "Name=instance-state-code,Values=16" \
--output text --query 'Reservations[*].Instances[*].PublicDnsName')

echo "ip address(es) of running instance(s) which has $INSTANCE_TAG tag: $INSTANCE"

echo "$INSTANCE" > /tmp/ip_file
echo "$INSTANCE_TAG" > /tmp/tag_file
echo "$INSTANCE_PUBLIC_DNS" > /tmp/public_dns_file
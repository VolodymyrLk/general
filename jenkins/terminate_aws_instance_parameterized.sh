#!/bin/bash

# initialize variables to default values
INSTANCE_IP="$STRING_PARAMETER_IP_ADDRESS" ## default tag of instance

echo "instance ip address is $INSTANCE_IP"

# gettig instance-ids based on input parameter (ip address) and terminating appropriate instance-ids
aws ec2 describe-instances \
--filters "Name=ip-address,Values=$INSTANCE_IP" \
--query 'Reservations[*].Instances[*].[InstanceId]' \
--output text |
while read line;
do aws ec2 terminate-instances --instance-ids  $line
done
#!/bin/sh
# set debug mode
set -x
# output log of userdata to /var/log/user-data.log
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
#set variables
s3_name=instances-data-673733667
greprc=$(aws s3 ls | grep $s3_name | wc -l)
#getting time, just for log file
echo "date"
sleep 25
#getting current local ipv4 host address
curl http://169.254.169.254/latest/meta-data/local-ipv4 > local-ipv4
#create bucket if not hasn't created yet
if [ $greprc -gt 0 ] ; then
  echo "The bucket $s3_name has found"
else
  aws s3 mb s3://$s3_name
  echo "the bucket $s3_name has not found; making $s3_name bucket"
fi
#copying current local ipv4 host address to se bucket
aws s3 cp local-ipv4 s3://instances-data-673733667/local-ipv4-zabbixserver
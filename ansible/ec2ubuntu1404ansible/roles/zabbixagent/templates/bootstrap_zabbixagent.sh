#!/usr/bin/env bash

zabbixserver=$(aws s3 cp s3://instances-data-673733667/local-ipv4-zabbixserver -)
echo "$zabbixserver"
# listenport=10050
zabbixagent_hostname=$(hostname)

#updating configuration for zabbix agent
# sed -i 's/# DebugLevel=3/DebugLevel=3/g' /etc/zabbix/zabbix_agentd.conf
# sed -i 's/# EnableRemoteCommands=0/EnableRemoteCommands=1/g' /etc/zabbix/zabbix_agentd.conf
# sed -i 's/# LogRemoteCommands=0/LogRemoteCommands=1/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/Server=127.0.0.1/Server='$zabbixserver'/g' /etc/zabbix/zabbix_agentd.conf
# sed -i 's/# ListenPort=10050/ListenPort='$listenport'/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/ServerActive=127.0.0.1/ServerActive='$zabbixserver'/g' /etc/zabbix/zabbix_agentd.conf
sed -i 's/Hostname=Zabbix server/Hostname='$zabbixagent_hostname'/g' /etc/zabbix/zabbix_agentd.conf

service zabbix-agent restart
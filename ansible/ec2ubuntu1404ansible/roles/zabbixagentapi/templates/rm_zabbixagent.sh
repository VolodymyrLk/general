#!/usr/bin/env bash

# set debug mode
# set -x

zabbixserver_user=admin
zabbixserver_pass=zabbix
api=http://$(aws s3 cp s3://instances-data-673733667/local-ipv4-zabbixserver -)/zabbix/api_jsonrpc.php

zabbixagent_hostname=$(hostname)

#authentication with zabbix api
authenticate() {
  echo `curl -s -H  'Content-Type: application/json-rpc' -d "{\"jsonrpc\": \"2.0\",\"method\":\"user.login\",\"params\":{\"user\":\""${zabbixserver_user}"\",\"password\":\""${zabbixserver_pass}"\"},\"auth\": null,\"id\":0}" $api`
}

auth_token=`echo $(authenticate) | jq -r .result`
echo "$auth_token"

gethostid() {
  echo `curl -s -H 'Content-Type: application/json-rpc' -d "{
      \"jsonrpc\": \"2.0\",
      \"method\":\"host.get\",
      \"params\":{
          \"output\":\"extend\",
          \"filter\":{
              \"host\":[\""${zabbixagent_hostname}"\"]
          }
      },
      \"auth\":\""${auth_token}"\",
      \"id\":0
  }" $api`
}

host_id=`echo $(gethostid) | jq -r .result[0].hostid`

removehost() {
  echo `curl -s -H 'Content-Type: application/json-rpc' -d "{
      \"jsonrpc\": \"2.0\",
      \"method\":\"host.delete\",
      \"params\":[\""${host_id}"\"],
      \"auth\":\""${auth_token}"\",
      \"id\":0
  }" $api`
}

echo "$(removehost)"
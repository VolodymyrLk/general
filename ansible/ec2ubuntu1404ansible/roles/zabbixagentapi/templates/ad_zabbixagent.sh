#!/usr/bin/env bash

# set debug mode
# set -x

zabbixserver_hostname=zabbixserver
zabbixserver_user=admin
zabbixserver_pass=zabbix
zabbixserver_ip=$(aws s3 cp s3://instances-data-673733667/local-ipv4-zabbixserver -)
api=http://$(aws s3 cp s3://instances-data-673733667/local-ipv4-zabbixserver -)/zabbix/api_jsonrpc.php

#host group where has to be created agent
hostgroupid=10
#template id which should be assigned
templateid_linux=10001
templateid_http=10094
templateid_tomcat=10168

zabbixagent_ip=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
zabbixagent_hostname=$(hostname)

#authentication with zabbix api
authenticate() {
  echo `curl -s -H  'Content-Type: application/json-rpc' -d "{\"jsonrpc\": \"2.0\",\"method\":\"user.login\",\"params\":{\"user\":\""${zabbixserver_user}"\",\"password\":\""${zabbixserver_pass}"\"},\"auth\": null,\"id\":0}" $api`
}

auth_token=`echo $(authenticate) | jq -r .result`
echo "$auth_token"

#monitoring host creation with zabbix api
create_host() {
  curl -i -X POST -H 'Content-Type: application/json-rpc' -d "{
    \"jsonrpc\":\"2.0\",
    \"method\":\"host.create\",
    \"params\":{
      \"host\":\"$zabbixagent_hostname\",
      \"interfaces\": [{
        \"type\": 1,
        \"main\": 1,
        \"useip\": 1,
        \"ip\": \"$zabbixagent_ip\",
        \"dns\": \"\",
        \"port\": \"10050\"
      }],
      \"groups\": [{
        \"groupid\": \"10\"
      }],
      \"templates\":[{
        \"templateid\": "$templateid_linux"
      }]
    },
  \"auth\":\"$auth_token\",
  \"id\":0
  }" $api
}

echo "$(create_host)" > ad_zabbixagent.conf
#!/usr/bin/env bash

# installing jq
#sudo apt-get update && install -y jq

# set debug mode
#set -x

# output log of userdata to /var/log/user-data.log
#exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# initialize variables to default values
INSTANCE_QUANTITY="1" ## default quantity of instances which we are creating
INSTANCE_TYPE="t2.micro" ## default type of instance
INSTANCE_TAG="ApacheTomcat" ## default tag of instance

# help function
function HELP {
  echo "---------------"
  echo "possible arguments:"
  echo "-h - showing help and exit"
  echo "-c - quantity of instances which we are creating"
  echo "-t - type of instance; can be: t2.nano, t2.micro, t2.small, t2.medium, t2.large, t2.xlarge, t2.2xlarge"
  echo "-v - tag of instance"
  exit 1
}

# help2 function
function HELP2 {
  echo "the number of arguments must be greater than 0"
  exit 1
}

# check the number of arguments. If none are passed, print help and exit.
NUMARGS=$#
echo -e \\n"number of arguments: $NUMARGS"
if [ $NUMARGS -eq 0 ]; then
  HELP2
fi

if ! echo "$@" | grep -q "-"; then
  echo "the line must begin with '-' "
  HELP
fi

# getting values of arguments at runtime
while getopts :hc:t:v: FLAG; do
    case $FLAG in
        h)  #show help
            HELP
            ;;
        c)  #set option "c"
            INSTANCE_QUANTITY=$OPTARG
            echo "-c used: $OPTARG"
            echo "INSTANCE_QUANTITY = $INSTANCE_QUANTITY"
            ;;
        t)  #set option "t"
            INSTANCE_TYPE=$OPTARG
            echo "-t used: $OPTARG"
            echo "INSTANCE_TYPE = $INSTANCE_TYPE"
            ;;
        v)  #set option "v"
            INSTANCE_TAG=$OPTARG
            echo "-v used: $OPTARG"
            echo "INSTANCE_TAG = $INSTANCE_TAG"
            ;;
        \?) #unrecognized option - show help
            echo "option -$OPTARG not allowed"
            HELP
            ;;
        :)
            echo "option -$OPTARG requires an argument"
            HELP
            ;;
    esac
done

shift $((OPTIND-1))  #this tells getopts to move on to the next argument

echo "---------------"
echo "quantity of aws ec2 instances: "$INSTANCE_QUANTITY
echo "type of instance: "$INSTANCE_TYPE
echo "instance tag: "$INSTANCE_TAG
echo "---------------"

EC2_INSTANCE_ID=`aws ec2 run-instances --region eu-central-1 --image-id ami-7c412f13 \
--count $INSTANCE_QUANTITY \
--instance-type $INSTANCE_TYPE \
--key-name jenkins \
--security-group-ids sg-c9e97fa4 \
--subnet-id subnet-c7168fbd \
--tag-specifications 'ResourceType=instance,Tags=[{Key=JenkinsSlaveType,Value='$INSTANCE_TAG'}]' | jq -r '.Instances[].InstanceId'`

echo "EC2 InstanceID: "$EC2_INSTANCE_ID
echo "---------------"

echo "waiting for instance-running"
aws ec2 wait instance-running --instance-ids $EC2_INSTANCE_ID
echo "instance is running"
echo "---------------"

INSTANCE=$(aws ec2 describe-instances --filters "Name=tag:JenkinsSlaveType,Values=$INSTANCE_TAG" "Name=instance-state-code,Values=16" \
--output text --query 'Reservations[*].Instances[*].PublicIpAddress')

echo "ip address(es) of running instance(s) which has $INSTANCE_TAG tag: $INSTANCE"

exit 0
#!/bin/bash

echo "positional parameters"
echo '$1 = ' $1

if [ "$1" != "" ]; then
    echo "set ip address"
    var1=$1
else
    echo "please, specify ip address"
    exit 1
    var1=1
fi

echo "value of instances"
echo 'var1 = ' $var1

# gettig instance-ids based on input parameter (ip address) and terminating appropriate instance-ids
aws ec2 describe-instances \
--filters "Name=ip-address,Values=$var1" \
--query 'Reservations[*].Instances[*].[InstanceId]' \
--output text |
while read line;
do aws ec2 terminate-instances --instance-ids  $line
done
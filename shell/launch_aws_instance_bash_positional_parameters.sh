#!/bin/bash

echo "positional parameters"
echo '$1 = ' $1

if [ "$1" != "" ]; then
    echo "set specified value of instances"
    var1=$1
else
    echo "count of instances hasn't set, using default value"
    var1=1
fi

echo "value of instances"
echo 'var1 = ' $var1

aws ec2 run-instances --region eu-central-1 --image-id ami-7c412f13 \
--count $var1 \
--instance-type t2.micro \
--key-name jenkins \
--security-group-ids sg-c9e97fa4 \
--subnet-id subnet-c7168fbd \
--tag-specifications 'ResourceType=instance,Tags=[{Key=JenkinsSlaveType,Value=ApacheTomcatRepo}]' |

while [ ! "aws ec2 describe-instances --filters "Name=tag:JenkinsSlaveType,Values=ApacheTomcatRepo" "Name=instance-state-code,Values=16"" ]

do
echo "instance pending"
done

echo "instance is up and running"

sleep 20

INSTANCE=$(aws ec2 describe-instances --filters "Name=tag:JenkinsSlaveType,Values=ApacheTomcatRepo" "Name=instance-state-code,Values=16" \
--output text --query 'Reservations[*].Instances[*].PublicIpAddress')

echo "instance ip is $INSTANCE"
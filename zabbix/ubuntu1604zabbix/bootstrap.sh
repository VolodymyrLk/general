#!/usr/bin/env bash

db_name=dbzabbix
db_user=root
db_pass=zabbix
front_user=admin
front_pass=zabbix

echo ""
echo "------------------------------"
echo "getting zabbix and dpkg"
#adding repositories and installing packages
wget http://repo.zabbix.com/zabbix/3.5/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.5-1+xenial_all.deb
#installing local *.deb file using
dpkg -i 'zabbix-release_3.5-1+xenial_all.deb'

echo ""
echo "------------------------------"
echo "update packages lists"
apt-get -y update

echo ""
echo "------------------------------"
echo "preparing mysql configuration and installing mysql server"
#disabling interactive mode when we manually entering parameters of user, password, etc.
export DEBIAN_FRONTEND=noninteractive
echo 'mysql-server mysql-server/root_password password zabbix' | debconf-set-selections
echo 'mysql-server mysql-server/root_password_again password zabbix' | debconf-set-selections
apt-get -y install mysql-server mysql-client

echo ""
echo "------------------------------"
echo "installing zabbix server, zabbix frontend php, zabbix agent"
apt-get -y install zabbix-server-mysql zabbix-frontend-php zabbix-agent

echo ""
echo "------------------------------"
echo "starting mqsql and creating zabbix db"
#starting the db service, creating db and tables
service mysql start
cd /usr/share/doc/zabbix-server-mysql
echo "create database $db_name;" | mysql -uroot -pzabbix
#creating tables inside of created database
#ps: in order to check if db/tables were created use: mysql -uroot -p > enter root password > show databases; > USE dbname; > show tables;
zcat create.sql.gz | mysql -uroot -pzabbix $db_name

echo ""
echo "------------------------------"
echo "updating zabbix configuration"
#updating configuration for zabbix
sed -i 's/DBName=zabbix/DBName='$db_name'/g' /etc/zabbix/zabbix_server.conf
sed -i 's/# DBPassword=/DBPassword=zabbix/g' /etc/zabbix/zabbix_server.conf
sed -i 's/DBUser=.*/DBUser=root/g' /etc/zabbix/zabbix_server.conf
sed -i "s/Hostname=Zabbix server/Hostname=`hostname`/" /etc/zabbix/zabbix_agentd.conf

echo ""
echo "------------------------------"
echo "starting zabbix server"
#starting zabbix server
service zabbix-server start

echo ""
echo "------------------------------"
echo "setting up php configuration"
#setting up php configuration
sed -i 's/post_max_size = 8M/post_max_size = 16M/'  /etc/apache2/conf-enabled/zabbix.conf
sed -i 's/max_execution_time = 30/max_execution_time = 300/' /etc/apache2/conf-enabled/zabbix.conf
sed -i 's/max_input_time = 60/max_input_time = 300/' /etc/apache2/conf-enabled/zabbix.conf
sed -i 's/# php_value date.timezone Europe\/Riga/php_value date.timezone Europe\/Kiev/' /etc/apache2/conf-enabled/zabbix.conf

echo ""
echo "------------------------------"
echo "restarting apache web server"
#starting zabbix http service
service apache2 restart

echo "------------------------------"
echo "db name: $db_name db user: $db_user, db pass: $db_pass"
echo ""
echo "front user: $front_user, front pass: $front_pass"
echo "------------------------------"